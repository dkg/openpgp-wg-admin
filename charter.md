OpenPGP is an Internet standard that covers object encryption, object signing, and identity certification.

The working group is chartered to work on improvements and additions to the OpenPGP format and ecosystem to address certain issues that have been identified by the community, as set out in the list of "in scope" topics below.
Due to the WG having been dormant for a number of years, there is somewhat of a "backlog" of topics, and as addressing all of these topics at once seems difficult, the WG will follow the process defined below to prioritize current lists of milestones, selected from this long list of "in-scope" topics.

# In-scope Topics

The working group will produce a number of specifications that are adjacent to the OpenPGP specification and provide guidance to OpenPGP libraries and/or applications.
These improvements may include:

## Security improvements

- **PQC**: The addition and facilitation of post-quantum algorithms for encryption and signing; e.g., by using [draft-wussler-openpgp-pqc](https://datatracker.ietf.org/doc/draft-wussler-openpgp-pqc/) as initial input.

- **Forward secrecy**: enable encrypted OpenPGP communication that cannot be decrypted when long-term keys are compromised.

- **Context binding**: facilitate [domain separation for signing and/or encryption](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/145).

## New functionality

- **Automatic Forwarding**: using proxy re-encryption; e.g., by using [draft-wussler-openpgp-forwarding](https://datatracker.ietf.org/doc/draft-wussler-openpgp-forwarding/) as initial input.

- **Persistent Symmetric Keys**: for long-term storage of symmetric key material, symmetrically encrypted messages, and symmetric attestations; e.g., by using [draft-huigens-openpgp-persistent-symmetric-keys](https://datatracker.ietf.org/doc/draft-huigens-openpgp-persistent-symmetric-keys/) as initial input.

- **1PA3PC**: First-Party Approval of Third-Party Certifications, to mitigate certificate flooding attacks; e.g., by using [draft-dkg-openpgp-1pa3pc](https://datatracker.ietf.org/doc/draft-dkg-openpgp-1pa3pc) as initial input.

- **Superseded Keys**: to facilitate [transition to new keys without revoking the old ones](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/222).

- **SOP**: Stateless OpenPGP Interface; e.g., by using [draft-dkg-openpgp-stateless-cli](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/) as initial input.

- **PGP/MIME Separate Encrypted Parts**: Extending [RFC 3156](https://datatracker.ietf.org/doc/rfc3156/) to describe [messages composed of multiple encrypted parts](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/171).

- **cert-d**: A common certificate storage mechanism; e.g., by using [draft-nwjw-openpgp-cert-d/](https://datatracker.ietf.org/doc/draft-nwjw-openpgp-cert-d/) as initial input.

## Network-based Key Discovery Mechanisms

- **HKP**: HTTP Keyserver Protocol; e.g., by using [draft-shaw-openpgp-hkp](https://datatracker.ietf.org/doc/draft-shaw-openpgp-hkp/) as initial input.

- **WKD**: Web Key Directory; e.g., by using [draft-koch-openpgp-webkey-service](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/) as initial input.

## Key Verification Mechanisms

- **WoT**: Specifying semantics for the Web-of-Trust calculus; e.g., by using [the OpenPGP Web of Trust draft](https://sequoia-pgp.gitlab.io/sequoia-wot/) as initial input.

- **Key Transparency**: in collaboration with the [Key Transparency Working Group](https://datatracker.ietf.org/wg/keytrans/about/), e.g., integrating its outputs.

- **Key Verification**: Improved manual key verification, for example using a QR code.

## Miscellaneous Cleanup Work

- **Semantics**: Define semantics of mechanisms provided by OpenPGP.
  This includes, but is not limited to, defining validity of signatures, acceptance and placement of signature subpackets, as well as structure and meaning of certificates and messages.

- **User ID Conventions**: Properly document User ID conventions; e.g., by using [draft-dkg-openpgp-userid-conventions](https://datatracker.ietf.org/doc/draft-dkg-openpgp-userid-conventions) as initial input.

- **Revocation**: Clarify and improve revocation semantics and workflows, including replacement of the deprecated Revocation Key mechanism; e.g., by using [draft-dkg-openpgp-revocation](https://datatracker.ietf.org/doc/draft-dkg-openpgp-revocation) as initial input.

- **Message Grammar**: [Simplify](https://mailarchive.ietf.org/arch/msg/openpgp/uepOF6XpSegMO4c59tt9e5H1i4g/) the OpenPGP Message Grammar; e.g., by limiting nesting, or by constraining sequences of packet types.

- **PGP/MIME One-Pass Signatures**: Extending [RFC 3156](https://datatracker.ietf.org/doc/rfc3156/) to permit [one-pass signature verification for v6 signatures](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/116)

# Working Group Process

The working group will endeavor to complete most of its work online on the working group's mailing list.
The group may hold face-to-face sessions at IETF meetings, or interim calls, whenever necessary or useful.

For all work items, any content requires both rough consensus on the mailing list and the demonstration of interoperable support by at least two independent implementations, before being submitted to the IESG.

The WG chairs will periodically poll the WG for which of the topics above to add as milestones, as participant interest, document editor, review and implementer resources permit.
The WG chairs will ensure that the list of active topics is credible at all times, likely resulting in only 3-4 topics being "active" at any given time.
Such polls will typically result in the addition of a call-for-adoption for one or two new topics from the list above, depending on resources, and as currently active work is completed.
"Completion" will typically mean that a draft has passed working group last call or IETF last call, but, if needed, the chairs will make use of other datatracker [IETF document states](https://datatracker.ietf.org/doc/help/state/draft-stream-ietf/), e.g., "parked WG document", if some document is stalled for technical or personnel reasons, leaving space for tackling anther topic.
